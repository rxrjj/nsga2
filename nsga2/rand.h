/* Declaration for random number related variables and routines */

# ifndef _RAND_H_
# define _RAND_H_

#include "config.h"

/* Variable declarations for the random number generator */
#ifdef NSGA2_USE_SYSTEM_RAND
extern unsigned int seed;
#else
extern double seed;
extern double oldrand[55];
extern int jrand;
#endif

/* Function declarations for the random number generator */
void randomize(void);
double randomperc(void);
int rnd (int low, int high);
double rndreal (double low, double high);

#ifdef NSGA2_USE_SYSTEM_RAND
void set_rand_seed(unsigned int sd);
#else
void set_rand_seed(double sd);
void warmup_random (double seed);
void advance_random (void);
#endif

# endif
