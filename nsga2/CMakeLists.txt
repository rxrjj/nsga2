

#message("CMAKE_C_FLAGS2=${CMAKE_C_FLAGS}")
aux_source_directory(. DIR_LIB_SRCS)
add_library(nsga2 SHARED ${DIR_LIB_SRCS})

install(TARGETS nsga2 DESTINATION lib)
