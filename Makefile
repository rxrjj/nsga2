# Makefile for compiling NSGA-II source code
PREFIX=/home/jrong/lib/packages/nsga2
#CC=gcc
#LD=gcc
RM=rm -f
CFLAGS= -shared -fPIC -Wall -ansi -pedantic -g -o3
OBJS:=$(patsubst %.c,%.o,$(wildcard ./nsga2/*.c))
TEST:=$(patsubst %.c,%.o,$(wildcard *.c))
MAIN=nsga2r
all:$(MAIN)
$(MAIN): shared_lib $(TEST)
	$(CC) $(LDFLAGS) $(TEST) -o $(MAIN) -L. -Wl,-rpath=. -lnsga2 -lm

%.o: %.c global.h rand.h
	$(CC) $(CFLAGS) -c $<

shared_lib:$(OBJS)
	$(CC) -shared $(OBJS) -o libnsga2.so

install: shared_lib
	if [ ! -d "$(PREFIX)" ]; then mkdir $(PREFIX); fi
	if [ ! -d "$(PREFIX)/lib" ]; then mkdir $(PREFIX)/lib; fi
	if [ ! -d "$(PREFIX)/include" ]; then mkdir $(PREFIX)/include; fi
	if [ ! -d "$(PREFIX)/include/nsga2" ]; then mkdir $(PREFIX)/include/nsga2; fi
	cp ./nsga2/*.h $(PREFIX)/include/nsga2
	cp *.so $(PREFIX)/lib


clean:
	$(RM) *.so $(OBJS) nsga2r

